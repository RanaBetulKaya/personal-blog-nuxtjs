import {defineStore} from 'pinia'

export const useProductStore=defineStore('productStore',{
    state:()=>({
        products:[],
        name:'My Basket',
    }),
    actions:{
        addProduct(item){
            console.log("aldığımız count:",item)
            const existingItem = this.products.find(p => {
                return p.id === item.id
            })
            console.log('Mevcut Item '+existingItem)
            if(existingItem){
                item.count++;
            }
            else{
                if(item.count===''){
                    item.count=1;
                    this.products.push(item)
                    alert("Product has been added successfully!")
                }
                else{
                    if(item.count>item.stock)
                        alert("Ups! There is only "+item.stock+" products that we have in stock")
                    else{
                        this.products.push(item)
                        alert("Product has been added successfully!")
                    }
                }
            }
            console.log(this.products)
        },
        deleteProduct(item){
            this.products=this.products.filter(p=>{
                item.count=0;
                return p.id!==item.id
            })
            console.log(this.products)
        },
        incrementCount(item){
            item.count++;
            console.log(this.products)
        },
        decrementCount(item){
            if(item.count!==0)
            item.count--;
            console.log(this.products)
        },
        totalPrice(){
            var total=0;
            for(var i=0;i<this.products.length;i++){
                var totalItem=this.products[i].price*this.products[i].count;
                total+=totalItem;
                console.log(totalItem)
            }
            console.log(total)
            return total
        }
    },
    getters:{
        
    },
})

